FROM rocker/shiny
LABEL Name=doe-contracting Version=0.0.1 
RUN apt-get update && apt-get install -y \
  libssl-dev
RUN install2.r --error \
  -r 'https://cran.rstudio.com' \
  ghit \
  && R -e 'ghit::install_github("jjallaire/sigma")'
COPY . /srv/shiny-server/networks
WORKDIR /srv/shiny-server/networks
